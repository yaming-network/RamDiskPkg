RamDiskPkg
## 介绍

一种将系统引导`UEFI`分区集成到固件中的技术。

## 编译环境
- 支持OC的AUDK以及EDK2构建
- 已验证支持EDK2
- 已验证支持OpenCore专用构建依赖（实用macOS Xcode工具链构建必须实用该依赖）
- 
## 构建教程
### xcode依赖ocmtoc而不是mtoc
### Xcode
```bash
git clone --depth=1 https://github.com/acidanthera/audk UDK 
cd UDK 
git submodule update --init --recommend-shallow 
rm -rf OpenCorePkg 
https://gitee.com/yaming-network/RamDiskPkg.git
. ./edksetup.sh 
make -C BaseTools 
build -a X64 -b RELEASE -t XCODE5 -p RamDiskPkg/RamDiskPkg.dsc
```
### GCC
